import React, { useState } from 'react';
import { BrowserRouter, Link, Navigate, Route, Routes } from 'react-router-dom';
import { ContextDemo } from './pages/ContextDemo';
import { UsersDetails } from './pages/crud/UsersDetails';
import ProductsPage from './pages/ProductsPage';
import { StateDemo } from './pages/StateDemo';
import { Panel } from './shared/Panel';
import { TabBar } from './shared/TabBar';
import { ContactsPage } from './pages/contacts/ContactsPage';
import CrudExamplePage from './pages/crud/CrudExamplePage';
import PanelDemoPage from './pages/PanelDemoPage';
import TabbarDemoPage from './pages/TabbarDemoPage';
import create from 'zustand'

type Page = 'contacts' | 'panel-demo' | 'crud' | 'tabbar-demo';

interface PageTab {
  id: number;
  name: string;
  url: Page;
}
const pages:PageTab[] = [
  { id: 1, name: 'Panel demo', url: 'panel-demo' },
  { id: 2, name: 'Contacts', url: 'contacts' },
  { id: 3, name: 'Crud', url: 'crud' },
  { id: 4, name: 'Tabbar', url: 'tabbar-demo' },
]


interface AppState {
  counter: number;
  theme: string;
  inc: () => void;
  changeTheme: (val: string) => void;
}
export const store = create<AppState>((set) => ({
  counter: 10,
  theme: 'dark',
  inc: () => set((state) => ({ counter: state.counter + 1 })),
  changeTheme: (val: string) => {
    set(() => ({ theme: val}))
  }
}))


const App = () => {
  // const page: 'contacts' | 'panel-demo' | 'crud' = 'contacts';
  const [page, setPage] = useState<Page>('crud')
  const theme = store(state => state.theme)

  return (
   <BrowserRouter>
     <div>Logo {theme}</div>
     <Link to="/panel-demo">
       <button>Panel demo</button>
     </Link>
     <Link to="users">users</Link> |
     <Link to="tabbar-demo">Tabbar</Link> |
     <Link to="contacts">Contacts</Link> |
     <Link to="products">products</Link>  |
     <Link to="context">context</Link> |
     <Link to="zustand">zustand</Link>

     <hr/>

     <Routes>
       <Route path='panel-demo' element={ <PanelDemoPage />} />
       <Route path='users' element={ <CrudExamplePage />} />
       <Route path='users/:id' element={ <UsersDetails />} />
       <Route path='tabbar-demo' element={ <TabbarDemoPage />} />
       <Route path='contacts' element={ <ContactsPage />} />
       <Route path='context' element={ <ContextDemo />} />
       <Route path='products' element={ <ProductsPage />} />
       <Route path='zustand' element={ <StateDemo />} />
       <Route index element={ <div>select a page</div>} />
       <Route path="*" element={
         <Navigate to="/" />
       } />
     </Routes>


    </BrowserRouter>
  )
}

export default App;


