import cn from 'classnames';
import React, {PropsWithChildren} from 'react';

interface PanelProps {
  title: string;
  icon?: string;
  url?: string;
  alert?: boolean;
  success?: boolean;
  onIconClick?: () => void;
}
export function Panel(props: PropsWithChildren<PanelProps>) {
  return <div className="card">
    <div className={cn('card-header', {'bg-danger': props.alert, 'bg-success': props.success})}>
      <span>{props.title}</span>

      {
        props.icon ?
          <div className="pull-right">
            <i className={props.icon}
               onClick={props.onIconClick}/>
          </div> : null
      }

    </div>

    {
      props.children &&
        <div className="card-body">
          {props.children}
        </div>
    }
  </div>
}







/*

class DataCollection<T> {
  items: T[] = [];
  add(item: T) {
    this.items.push(item)
  }
  getData() {
    return this.items;
  }
}

const d = new DataCollection<number>()
d.add('ciao')
d.add(123)
console.log(d.getData()[0].)

*/






