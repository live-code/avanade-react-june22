import { User } from '../model/user';
import cn from 'classnames';
import React from 'react';

interface UserCardProps {
  user: User
}

export function UserCard(props: UserCardProps) {
  function getName(u: User) {
    return <h1>{u.name + u.id}</h1>
  }

  return (
    <div className="card">
      <div
        className={cn('card-header', {
          'male': props.user.gender === 'M',
          'female': props.user.gender === 'F',
          'abc': props.user.gender === 'ABC'
        })}
      > {getName(props.user)} </div>
      <div className="card-body">
        <img
          width="100%"
          src={`https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=${props.user.coords.lat},${props.user.coords.lng}&zoom=7&size=600,400`} alt=""/>
      </div>
    </div>

  )
}
