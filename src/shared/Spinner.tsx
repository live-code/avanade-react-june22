import React from 'react';

export function Spinner() {
  return <i className="fa fa-spinner fa-spin fa-3x fa-fw"></i>
}
