import { Product } from '../model/product';

interface ListProps {
  products: Product[];
  title?: string;
}

//export const List = (props: ListProps) => {
export const List = (props: ListProps) => {
  return <div>
    <h1>{props.title}</h1>
    ci sono { props.products.length } elementi
  </div>;
}

// export const getList = (p: any[]) => <div> there are {p.length} products</div>
