import { User } from '../model/user';

interface TabBarProps<T> {
  items: T[];
  onTabClick: (item: T) => void
}

export function TabBar<T extends { id: number, name: string}>(props: TabBarProps<T>) {
  return (
    <>
      <ul className="nav nav-tabs">
        {
          props.items.map(function (item) {
            return <li className="nav-item"
                       key={item.id}
                       onClick={() => props.onTabClick(item)}>
              <a className="nav-link active">{item.name}</a>
            </li>
          })
        }
      </ul>
    </>
  )
}
