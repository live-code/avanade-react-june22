import React from 'react';
import { TabBar } from '../shared/TabBar';
import { User } from '../model/user';

interface MyUser {
  id: number;
  name: string;
}

const users: MyUser[] = [
  { id: 11, name: 'Fabio'},
  { id: 12, name: 'Lorenzo'},
  { id: 13, name: 'Marco'},
]

const TabBarDemoPage = () => {

  function tabClickHandler(item: MyUser) {
    console.log('....', item)
  }

  return (
    <div>

      <TabBar<MyUser> items={users} onTabClick={tabClickHandler} />
    </div>
  )
}

export default TabBarDemoPage;

