import React from 'react';
import { Empty } from '../shared/Empty';
import { List } from '../shared/List'
import { Product } from '../model/product';

const data: Product[] = [
  { id: 1, title: 'Pane'},
  { id: 2, title: 'Nutella' },
];

const ProductsPage = () => {
  return (
    <div>
      {
        data.length > 0 ?
          <List products={data} />: <Empty />
      }

      <hr/>
      {
        data.length > 0 ?
          <List title="LISTA PRODOTTI" products={data} />: <Empty />
      }
    </div>
  )
}

export default ProductsPage;
