import React from 'react';
import { Panel } from '../shared/Panel';
import { UserCard } from '../shared/UserCard';
import { User } from '../model/user'

const user1: User = {
  id: 1,
  name: 'Fabio',
  gender: 'ABC',
  coords: {
    lat: 45,
    lng: 13
  }
};
const user2: User = {
  id:2,
  name: 'Lorenzo',
  gender: 'M',
  coords: {
    lat: 43,
    lng: 13
  }
};

const PanelDemoPage = () => {

  function openUrl() {
    window.open('http://www.google.com')
  }

  function log() {
    console.log('ciao')
  }

  return (
    <div className="container mt-3">
      <Panel
        icon="fa fa-facebook"
        title="Profilo"
        onIconClick={log}
      >
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi at atque consequuntur corporis dignissimos doloremque eius eos fugiat hic minus mollitia non odio, possimus, praesentium rem repudiandae similique tempore voluptatibus?
      </Panel>

      <Panel
        success icon="fa fa-google" title="Profilo"
        onIconClick={openUrl}
      >
        <button>ciao</button>
      </Panel>

      <Panel title="ALERT" alert>
       ahia! qualcosa è andato storto
      </Panel>

      <UserCard user={user1} />
      <UserCard user={user2} />
    </div>
  )
}

export default PanelDemoPage;

