import React, { useEffect, useState } from 'react';
import { store } from '../App';


export function StateDemo() {

  return  <div className="comp">
    Top
    <Parent />
  </div>
}

function Parent() {
  const inc = store(state => state.inc)
  const changeTheme = store(state => state.changeTheme)
  return <div className="comp">
    Parent
    <button onClick={inc}>+</button>
    <button onClick={() => changeTheme('light')}>light</button>
    <button onClick={() => changeTheme('dark')}>dark</button>
    <Child/>
  </div>
}


const Child = React.memo(() => {
  console.log('render')
  const count = store(state => state.counter)
  const thm = store(state => state.theme)
  return <div className="comp">
    Child {count}

  </div>
})
