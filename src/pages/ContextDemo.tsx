import React, { useContext, useRef, useState } from 'react';

interface DataContextProps {
  data: number,
  inc: () => void
}

export const DataContext = React.createContext<DataContextProps | null>(null)

export function ContextDemo() {
  const [data, setData] = useState<number>(10)

  function inc() {
    setData(c => c+1)
  }
  return  <DataContext.Provider value={{ data, inc }}>
    <Parent  />
    <button onClick={() => setData(c => c+1)}>+</button>
  </DataContext.Provider>
}

// parent.tsx
function Parent(props: any) {
  return <div className="comp">
    Parent
    <Child/>
  </div>
}


// child.ts
function Child(props: any) {
  const ctx = useContext(DataContext)
  return <div className="comp">
    Child {ctx?.data}
    <button onClick={ctx?.inc}>+</button>
  </div>
}
