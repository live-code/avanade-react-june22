import axios from 'axios';
import { useCallback, useEffect, useState } from 'react';
import { Member } from '../../../model/member';

const BASEPATH = 'http://localhost:3001'


export function useCrud() {
  const [error, setError] = useState<boolean>(false)
  const [pending, setPending] = useState<boolean>(false)
  const [list, setList] = useState<Partial<Member>[]>([]);
  const [selectedItem, setSelectedItem] = useState<Partial<Member> | null>(null)
  const [showModal, setShowModal] = useState<boolean>(false)

  useEffect(() => {
    getAll();
  }, [])

  function getAll() {
    setPending(true)
    axios.get<Member[]>(`${BASEPATH}/users`)
      .then(function(res) {
        setPending(false)
        setList(res.data)
      })
      .catch(() => {
        setError(true)
        setPending(false)
      })
  }


  const deleteHandler = useCallback((id: number) => {
    setPending(true)
    setError(false)
    axios.delete(`${BASEPATH}/users/${id}`)
      .then(() => {
        setPending(false)
        const newList = list.filter(item => item?.id !== id)
        setList(newList)
      })
      .catch(() => {
        setError(true)
        setPending(false)
      })
  }, [list])

  function save(member: Partial<Member>) {
    if(selectedItem?.id) {
      editUser(member)
    } else {
      addUser(member)
    }
  }

  function addUser(member: Partial<Member>) {
    setError(false)
    axios.post<Partial<Member>>(`${BASEPATH}/users/`, member )
      .then((res) => {
        setList([...list, res.data ])
        setSelectedItem(null);
        closeModal();
      })
  }

  function editUser(member: Partial<Member>) {

    axios.patch<Partial<Member>>(`${BASEPATH}/users/${selectedItem?.id}`, member )
      .then(res => {
        setList(
          list.map(item => item.id === selectedItem?.id ? res.data :  item)
        )
        setSelectedItem(null);
        closeModal();
      })
  }

  function selectUser(item: Partial<Member>) {
    setSelectedItem(item);
    openModal()
  }

  function openModal() {
    setShowModal(true)
  }
  function closeModal() {
    setShowModal(false)
  }

  return {
    error,
    loader: pending,
    list,
    selectedItem,
    showModal,
    actions: {
      getAll,
      save,
      deleteUser: deleteHandler,
      selectUser,
      closeModal,
      openModal
    },
  }
}
