import axios from 'axios';
import classNames from 'classnames';
import React, { useEffect, useRef, useState } from 'react';
import { Gender, Member } from '../../../model/member';

interface UserFormProps {
  user: Partial<Member> | null;
  onAddUser: (obj: Partial<Member>) => void
}
export function UserForm(props: UserFormProps) {
  const [formData, setFormData] = useState<Partial<Member>>({ name: '', phone: '', gender: ''});
  const [dirty, setDirty] = useState<boolean>(false);

  useEffect(() => {
    console.log(props.user)
    if (props.user) {
      setFormData(props.user)
    }
  }, [props.user])


  function save() {
    props.onAddUser(formData)
  }

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    const key: string = e.currentTarget.name;
    const value: string = e.currentTarget.value;
    setFormData({ ...formData, [key]: value});
    setDirty(true);
  }

  // derived state
  const isNameValid = formData.name && formData.name.length > 3 ;
  const isPhoneValid = formData.phone !== '' ;
  const valid =  isNameValid && isPhoneValid;

  return <div>
    <input type="text"
           className={classNames('form-control', {'is-invalid': !isNameValid && dirty, 'is-valid': isNameValid} )}
           placeholder="name" name="name" value={formData.name} onChange={onChangeHandler}/>
    <input type="text"
           className={classNames('form-control', {'is-invalid': !isPhoneValid && dirty, 'is-valid': isPhoneValid} )}
           placeholder="phone" name="phone" value={formData.phone} onChange={onChangeHandler}/>

    <select
      value={formData.gender} name="gender" onChange={onChangeHandler}>
      <option value="">Select gender</option>
      <option value="M">Male</option>
      <option value="F">FeMale</option>
    </select>

    <br/>
    <button onClick={save} disabled={!valid}>save</button>
  </div>
}
