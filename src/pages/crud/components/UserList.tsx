import React, { useDeferredValue, useState } from 'react';
import { Link } from 'react-router-dom';
import { Member } from '../../../model/member';

interface UserListProps {
  items: Partial<Member>[];
  sort: 'ASC' | 'DESC';
  onSelectUser: (item: Partial<Member>) => void
  onDeleteUser: (id: number) => void
}
export const UserList = React.memo((props: UserListProps) => {

  // const list = useDeferredValue(props.items); // low priority => react 18 => performance optimization

  function deleteHandler(id: number | undefined) {
    if (id) {
      props.onDeleteUser(id)
    }
  }

  function onItemCLickHandler(id: number) {
    console.log('onItemCLickHandler', id)
  }

  const ordererList = props.items.sort((item1: any, item2: any) => {
    return props.sort === 'ASC' ?
      item1.name.localeCompare(item2.name) :
      item2.name.localeCompare(item1.name)
  })

  return (
    <>
      <h3>{props.items.length} users</h3>


      <ul className="list-group">
        {
          ordererList.map(item => (
            <li key={item.id} className="list-group-item"
                onClick={() => props.onSelectUser(item)}>
              {item.name} - 📞 {item.phone}
              <i className="fa fa-trash"
                 onClick={() => deleteHandler(item.id)}></i>

              <Link to={`/users/${item.id}`}>
                <i className="fa fa-info-circle"></i>
              </Link>
            </li>
          ))
        }
      </ul>
    </>
  )
})
