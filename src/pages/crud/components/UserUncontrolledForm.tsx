import React, { useEffect, useRef } from 'react';
import { Member } from '../../../model/member';

interface UserFormProps {
  onAddUser: (obj: Partial<Member>) => void
}
export function UserUncontrolledForm(props: UserFormProps) {
  const inputNameEl = useRef<HTMLInputElement>(null)
  const inputPhoneEl = useRef<HTMLInputElement>(null)

  function isInvalid() {
    return (inputNameEl.current?.value === '' || inputPhoneEl.current?.value === '')
  }

  function keydownHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (isInvalid()) return;
    if (e.key === 'Enter') {
      props.onAddUser({ name: inputNameEl.current?.value, phone: inputPhoneEl.current?.value})
    }
  }

  function addUser() {
    if (isInvalid()) return;
    props.onAddUser({ name: inputNameEl.current?.value, phone: inputPhoneEl.current?.value})
  }

  return <div>
    <input ref={inputNameEl} type="text" className="form-control" placeholder="name" onKeyUp={keydownHandler}/>
    <input ref={inputPhoneEl} type="text" className="form-control" placeholder="phone" onKeyUp={keydownHandler}/>
    <button onClick={addUser} >ADD</button>
  </div>
}
