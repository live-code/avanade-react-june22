import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Member } from '../../model/member';

export function UsersDetails() {
  const params = useParams<{ id: string}>();

  const  [user, setUser] = useState<Partial<Member> | null>(null)

  useEffect(() => {
    axios.get('http://localhost:3001/users/' + params.id)
      .then(res=> setUser(res.data))
  }, [params.id]);

  return <div>
    {user?.name}
    {user?.phone}
  </div>
}
