import React, { useEffect, useState } from 'react';
import { Member } from '../../model/member';
import { Spinner } from '../../shared/Spinner';
import { UserForm } from './components/UserForm';
import { UserList } from './components/UserList';
import { useCrud } from './hooks/useCrud';


const CrudExamplePage = () => {
  const { error, loader, showModal, list, selectedItem, actions } = useCrud();
  const [sort, setSort] = useState<'ASC' | 'DESC'>('ASC');

  function filterHandler(e: React.ChangeEvent<HTMLSelectElement>) {
    setSort(e.currentTarget.value as 'ASC' | 'DESC')
  }
  return (
    <div>
      { error && <div>errore!</div>}
      { loader && <Spinner />}

      <i className="fa fa-plus-circle fa-2x" onClick={actions.openModal}></i>

      {
       showModal && <div className="my-modal">
          <UserForm
            user={selectedItem}
            onAddUser={actions.save}/>
          <hr/>
        <button onClick={actions.closeModal}>x</button>
        </div>
      }

      <select onChange={filterHandler}>
        <option value="ASC">asc</option>
        <option value="DESC">desc</option>
      </select>

      {/*<UserUncontrolledForm onAddUser={actions.addUser}/>*/}
      <UserList
        sort={sort}
        items={list}
        onDeleteUser={actions.deleteUser}
        onSelectUser={actions.selectUser}
      />
    </div>
  )
}

export default CrudExamplePage;
