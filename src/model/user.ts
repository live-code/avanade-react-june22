
// union type + literal types
export type Gender =  'M' | 'F' | 'ABC';

export interface Coords {
  lat: number;
  lng: number;
}


export interface User {
  id: number;
  name: string;
  gender: Gender;
  coords: Coords;
}


